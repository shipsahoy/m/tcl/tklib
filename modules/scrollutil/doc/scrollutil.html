<html>
<head>
  <title>Scrollutil Programmer's Guide</title>

  <meta name="Author" content="Csaba Nemethi">
  <meta name="Keywords" content=
  "scrollarea, scrollsync, mouse wheel event, binding, event handling, scrolling, scrollable widget container, focus">

  <link rel="stylesheet" type="text/css" href="stylesheet.css">
</head>

<body bgcolor="#FFFFFF">
  <div align="center">
    <h1>Scrollutil Programmer's Guide</h1>

    <h2>For Scrollutil Version 1.1</h2>

    <h3>by</h3>

    <h2>Csaba Nemethi</h2>

    <address>
      <a href="mailto:csaba.nemethi@t-online.de">csaba.nemethi@t-online.de</a>
    </address>
  </div>
  <hr>

  <h2 id="contents">Contents</h2>

  <h4><a href="#overview">Overview</a></h4>

  <ul>
    <li><a href="#ov_what">What Is Scrollutil?</a></li>

    <li><a href="#ov_get">How to Get It?</a></li>

    <li><a href="#ov_install">How to Install It?</a></li>

    <li><a href="#ov_use">How to Use It?</a></li>
  </ul>

  <h4><a href="#examples">Examples</a></h4>

  <ul>
    <li><a href="#ex_ScrolledTablelist">A Scrolled tablelist Widget</li>

    <li><a href="#ex_SyncListboxes">Synchronizing Two listbox Widgets</li>

    <li><a href="#ex_SyncTablelists">Synchronizing Three tablelist Widgets</li>

    <li><a href="#ex_ScrollableFrameDemo1">A Script Using a BWidget
    ScrollableFrame Widget</a></li>

    <li><a href="#ex_ScrolledFrameDemo1">A Script Using an
    iwidgets::scrolledframe Widget</a></li>

    <li><a href="#ex_ScrollableFrameDemo2">A Script Using Two BWidget
    ScrollableFrame Widgets</a></li>

    <li><a href="#ex_ScrolledFrameDemo2">A Script Using Two
    iwidgets::scrolledframe Widgets</a></li>
  </ul>

  <div align="center">
    <p><a href="index.html">Start page</a></p>
  </div>
  <hr>

  <h2 id="overview">Overview</h2>

  <h3 id="ov_what">What Is Scrollutil?</h3>

  <p>Scrollutil is a library package for Tcl/Tk versions 8.0 or higher, written
  in pure Tcl/Tk code.&nbsp; It contains:</p>
  
  <ul>
    <li>the implementation of the <a href=
    "scrollarea.html"><b>scrollarea</b></a> and <a href=
    "scrollsync.html"><b>scrollsync</b></a> mega-widgets, including a general
    utility module for mega-widgets;</li>

    <li>commands for <i>user-friendly</i> mouse wheel event handling in
    <b>scrollable widget containers</b> like BWidget ScrollableFrame and
    iwidgets::scrolledframe.&nbsp; These commands require Tcl/Tk versions 8.4
    or higher on X11 and Mac OS X and Tk 8.6b2 or later on Windows;</li>

    <li>demo scripts illustrating the use of the Scrollutil package in
    connection with various scrollable widgets and the above-mentioned
    scrollable widget containers;</li>

    <li>this tutorial;</li>

    <li>reference pages in HTML format.</li>
  </ul>

  <p><b>The scrollutil::scrollarea mega-widget</b> greatly simplifies the
  creation of arbitrary scrolled widgets.&nbsp; It consists of a scrollable
  widget and two scrollbars connected with that widget.&nbsp; The display mode
  of each scrollbar can be <code>static</code>, <code>dynamic</code>, or
  <code>none</code>.&nbsp; This scrolled window implementation also supports
  the widgets that are scrollable in one direction only (e.g., entry and
  ttk::entry) and respects the header component and title columns of <a href=
  "http://www.nemethi.de/tablelist/">tablelist</a> widgets (this is freely
  configurable).</p>
  
  <p>The scrollutil::scrollarea widget is similar to BWidget ScrolledWindow and
  its snit-based equivalent widget::scrolledwindow, contributed by Jeffrey
  Hobbs and contained in tklib.&nbsp; The snit-based <a href=
  "http://web.tiscali.it/irrational/tcl/scrodget-2.1/">scrodget</a> package by
  Aldo Buratti and its TclOO-based equivalent <a href=
  "https://wiki.tcl-lang.org/page/A+Scrolled+Widget+implemented+with+TclOO">scrolledwidget</a>
  contributed by Johann Oberdorfer are further scrolled window implementations.
  However, <i>full</i> tablelist support is only provided by the scrollarea
  widget, which is free from external dependencies like BWidget, snit, or (for
  Tcl 8.5) TclOO.&nbsp; It is also free from the <a href=
  "https://wiki.tcl-lang.org/page/Scroll+bars+that+appear+only+when+needed">shimmering
  problem in connection with text widgets</a>, which the above-mentioned
  scrolled window implementations either share with the autoscroll package
  (contained in tklib) or circumvent in a suboptimal way.</p>

  <p><b>The scrollutil::scrollsync mega-widget</b> is designed for scrolling
  several widgets simultaneously.&nbsp; Whenever the horizontal/vertical
  position of the view in the window of one of its widgets changes, the view in
  the windows of all the other widgets is automatically adjusted accordingly,
  thus making sure that the view's position in these windows is kept in
  sync.&nbsp; This mega-widget is horizontally and vertically scrollable, hence
  it can be embedded into a scrollutil::scrollarea widget.</p>

  <p>From the point of view of <b>the commands for mouse wheel event
  handling</b> provided by the Scrollutil package, the scrollability of a
  widget container window means that the associated Tcl command supports
  the&nbsp; <code>xview scroll <i>number</i> units</code>&nbsp; and&nbsp;
  <code>yview scroll <i>number</i> units</code>&nbsp; subcommands.&nbsp; The
  reason for requiring at least Tk version 8.6b2 on Windows for these commands
  is that in earlier Tk versions on this platform the mouse wheel events were
  sent to the widget having the focus rather than to the one under the
  pointer.</p>

  <p>To make use of the user-friendly mouse wheel event handling via the
  Scrollutil package, follow the steps below:</p>

  <ul>
    <li>Create mouse wheel event bindings for the binding tag
    <code>"all"</code> or for the toplevel widgets (including <code>"."</code>)
    having scrollable widget containers, by invoking the <code><a href=
    "wheelEvent.html#create">scrollutil::createWheelEventBindings</a></code>
    command.&nbsp; In addition, register your scrollable widget containers for
    scrolling via these bindings with the aid of the <code><a href=
    "wheelEvent.html#enable">scrollutil::enableScrollingByWheel</a></code>
    command.&nbsp; The above-mentioned bindings handle the mouse wheel events
    by scrolling the registered scrollable widget container that is an
    ascendant of the widget under the pointer and is contained in the latter's
    toplevel.</li>

    <li class="tm">Invoke the <code><a href=
    "wheelEvent.html#adapt">scrollutil::adaptWheelEventHandling</a></code>
    command for those widgets contained in registered scrollable widget
    containers that have mouse wheel event (class) bindings.&nbsp; This step
    eliminates the annoying and often dangerous double-handling effect, by
    modifying the mouse wheel event handling as follows:&nbsp; If the focus is
    on the widget under the pointer then the mouse wheel events will be handled
    by the (class bindings of the) widget only, otherwise by the bindings
    created with the <code>scrollutil::createWheelEventBindings</code>
    command.&nbsp; Without this step the mouse wheel events would scroll both
    the listbox, text, ttk::treeview, or tablelist widget under the pointer
    <i>and</i> the widget container to whose descendants the latter belongs, or
    they would select the next/previous value in the ttk::combobox or
    ttk::spinbox under the pointer <i>in addition to</i> scrolling the widget
    container.</li>

    <li class="tm">For some widgets it can be desirable to make the focus check
    within this modified event handling less restrictive.&nbsp; For example, if
    the widget under the pointer is an entry component of a <a href=
    "http://www.nemethi.de/mentry/">mentry</a> of type <code>"Date"</code>,
    <code>"Time"</code>, <code>"DateTime"</code>, <code>"IPAddr"</code>, or
    <code>"IPv6Addr"</code> and the focus is on any of its siblings, then the
    mouse wheel events sent to this entry should be handled by the mentry
    widget rather than scrolling the widget container that is an ascendant of
    the mentry.&nbsp; The <code><a href=
    "wheelEvent.html#setFocusCkWin">scrollutil::setFocusCheckWindow</a></code>
    command covers exactly cases like this.</li>
  </ul>

  <p>The mouse wheel event handling with the aid of the Scrollutil package was
  also tested to work with the <code>scrolledframe::scrolledframe</code>
  command of the Scrolledframe package by Maurice Bredelet (ulis) and its
  optimized and enhanced version contributed by Keith Nash, as well as with the
  <code>sframe</code> command implemented by Paul Walton.&nbsp; For details on
  these commands see the wiki page</p>

  <blockquote>
    <address>
      <a href=
      "https://wiki.tcl-lang.org/page/A+scrolled+frame">https://wiki.tcl-lang.org/page/A+scrolled+frame</a>
    </address>
  </blockquote>
  
  <p>These commands provide further implementations of scrollable widget
  containers.</p>

  <h3 id="ov_get">How to Get It?</h3>

  <p>Scrollutil is available for free download from the Web page</p>

  <blockquote>
    <address>
      <a href="http://www.nemethi.de">http://www.nemethi.de</a>
    </address>
  </blockquote>

  <p>The distribution file is <code>scrollutil1.1.tar.gz</code> for UNIX and
  <code>scrollutil1_1.zip</code> for Windows.&nbsp; These files contain the
  same information, except for the additional carriage return character
  preceding the linefeed at the end of each line in the text files for
  Windows.</p>

  <p>Scrollutil is also included in tklib, which has the address</p>

  <blockquote>
    <address>
      <a href="http://core.tcl.tk/tklib">http://core.tcl.tk/tklib</a>
    </address>
  </blockquote>

  <h3 id="ov_install">How to Install It?</h3>

  <p>Install the package as a subdirectory of one of the directories given by
  the <code>auto_path</code> variable.&nbsp; For example, you can install it as
  a directory at the same level as the Tcl and Tk script libraries.&nbsp; The
  locations of these library directories are given by the
  <code>tcl_library</code> and <code>tk_library</code> variables,
  respectively.</p>

  <p>To install Scrollutil <i>on UNIX</i>, <code>cd</code> to the desired
  directory and unpack the distribution file
  <code>scrollutil1.1.tar.gz</code>:</p>

  <blockquote>
    <pre>
gunzip -c scrollutil1.1.tar.gz | tar -xf -
</pre>
  </blockquote>

  <p>On most UNIX systems this can be replaced with</p>

  <blockquote>
    <pre>
tar -zxf scrollutil1.1.tar.gz
</pre>
  </blockquote>

  <p>Both commands will create a directory named <code>scrollutil1.1</code>,
  with the subdirectories <code>demos</code>, <code>doc</code>, and
  <code>scripts</code>.</p>

  <p><i>On Windows</i>, use WinZip or some other program capable of unpacking
  the distribution file <code>scrollutil1_1.zip</code> into the directory
  <code>scrollutil1.1</code>, with the subdirectories <code>demos</code>,
  <code>doc</code>, and <code>scripts</code>.</p>

  <p>Notice that in tklib the Scrollutil <code>demos</code> directory is
  replaced with the subdirectory <code>scrollutil</code> of the
  <code>examples</code> directory.&nbsp; Please take this into account when
  reading the <a href="#examples">examples</a> below.</p>

  <h3 id="ov_use">How to Use It?</h3>

  <p>The Scrollutil distribution provides two packages, called
  <b>Scrollutil</b> and <b>Scrollutil_tile</b>.&nbsp; The main difference
  between the two is that Scrollutil_tile enables the tile-based,
  theme-specific appearance of scrollarea widgets; this package requires Tcl/Tk
  8.4 or higher and tile 0.6 or higher.&nbsp; It is not possible to use both
  packages in one and the same application, because both are implemented in the
  same <code>scrollutil</code> namespace and provide identical commands.</p>

  <p>To be able to access the commands and variables defined in the package
  Scrollutil, your scripts must contain one of the lines</p>

  <blockquote>
    <pre>
package require scrollutil ?<i>version</i>?
package require Scrollutil ?<i>version</i>?
</pre>
  </blockquote>

  <p>You can use either one of the two statements above because the file
  <code>scrollutil.tcl</code> contains both lines</p>

  <blockquote>
    <pre>
package provide scrollutil ...
package provide Scrollutil ...
</pre>
  </blockquote>

  <p>Likewise, to be able to access the commands and variables defined in the
  package Scrollutil_tile, your scripts must contain one of the lines</p>

  <blockquote>
    <pre>
package require scrollutil_tile ?<i>version</i>?
package require Scrollutil_tile ?<i>version</i>?
</pre>
  </blockquote>

  <p>Again, you can use either one of the two statements above because the file
  <code>scrollutil_tile.tcl</code> contains both lines</p>

  <blockquote>
    <pre>
package provide scrollutil_tile ...
package provide Scrollutil_tile ...
</pre>
  </blockquote>

  <p>You are free to remove one of these two lines from
  <code>scrollutil.tcl</code> and <code>scrollutil_tile.tcl</code>,
  respectively, if you want to prevent the corresponding packages from making
  themselves known under two different names each.&nbsp; Of course, by doing so
  you restrict the argument of&nbsp; <code>package require</code>&nbsp; to a
  single name.</p>

  <p>Since the packages Scrollutil and Scrollutil_tile are implemented in the
  <code>scrollutil</code> namespace, you must either invoke the</p>

  <blockquote>
    <pre>
namespace import scrollutil::<i>pattern</i> ?scrollutil::<i>pattern ...</i>?
</pre>
  </blockquote>

  <p>command to import the <i>procedures</i> you need, or use qualified names
  like <code>scrollutil::scrollarea</code>.&nbsp; In the <a href=
  "#examples">examples</a> below we have chosen the latter approach.</p>

  <p>To access Scrollutil <i>variables</i>, you <i>must</i> use qualified
  names.&nbsp; There are only three Scrollutil variables that are designed to
  be accessed outside the namespace <code>scrollutil</code>:</p>

  <ul>
    <li>The variable <code>scrollutil::version</code> holds the current version
    number of the Scrollutil package.</li>

    <li>The variable <code>scrollutil::library</code> holds the location of the
    Scrollutil installation directory.</li>

    <li>The read-only variable <code>scrollutil::usingTile</code> has the value
    <code>0</code> in the package Scrollutil and the value <code>1</code> in
    Scrollutil_tile.</li>
  </ul>

  <p>The Scrollutil_tile package checks whether the required Tk and tile
  versions are present, by executing the commands</p>

  <blockquote>
    <pre>
package require Tk 8.4
if {$::tk_version &lt; 8.5 || [regexp {^8\.5a[1-5]$} $::tk_patchLevel]} {
    package require tile 0.6
}
</pre>
  </blockquote>

  <p>The second command above reflects the fact that, beginning with Tk 8.5a6,
  tile is integrated into the Tk core and therefore it should only be loaded
  explicitly when using an earlier Tk version.</p>

  <div align="center">
    <p><a href="#contents">Contents</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href=
    "index.html">Start page</a></p>
  </div>
  <hr>

  <h2 id="examples">Examples</h2>

  <h3 id="ex_ScrolledTablelist">A Scrolled tablelist Widget</h3>

  <p>This example shows how you can greatly simplify the creation of a scrolled
  tablelist by using a <a href="scrollarea.html">scrollarea</a> widget.</p>

  <p>The file <code>ScrolledTablelist1.tcl</code> in the <code>demos</code>
  directory creates a horizontally and vertically scrolled tablelist widget
  having two header rows and one title column, and manages the two scrollbars
  in such a way that the vertical scrollbar appears below the tablelist's
  header and the horizontal one starts to the right of the widget's title
  column area:</p>

  <blockquote>
    <img src="ScrolledTablelist.png" alt="ScrolledTablelist" width="481"
    height="359">
  </blockquote>

  <p>The script achieves these requirements using traditional scrollbar
  management, which is shown below in <span>red</span> color:</p>

  <blockquote>
    <pre>
package require tablelist_tile 6.3

wm title . "Scrolled Tablelist"

#
# Create the tablelist and the scrollbars as children
# of a frame having -borderwidth 1 and -relief sunken
#
set f   [ttk::frame .f]
set frm [ttk::frame $f.frm <span>-borderwidth 1 -relief sunken</span>]
set tbl $frm.tbl
<span>set vsb $frm.vsb
set hsb $frm.hsb</span>
tablelist::tablelist $tbl ... <span>-borderwidth 0</span> \
<span>    -xscrollcommand [list $hsb set] -yscrollcommand [list $vsb set]</span>
. . .
<span>ttk::scrollbar $vsb -orient vertical   -command [list $tbl yview]
ttk::scrollbar $hsb -orient horizontal -command [list $tbl xview]</span>

. . .

#
# Manage the widgets within the frame
#
<span>grid $tbl -row 0 -rowspan 2 -column 0 -columnspan 2 -sticky news
if {[tk windowingsystem] eq "win32"} {
    grid $vsb -row 0 -rowspan 2 -column 2 -sticky ns
} else {
    grid [$tbl cornerpath] -row 0 -column 2 -sticky ew
    grid $vsb              -row 1 -column 2 -sticky ns
}
grid [$tbl cornerpath -sw] -row 2 -column 0 -sticky ns
grid $hsb                  -row 2 -column 1 -sticky ew
grid rowconfigure    $frm 1 -weight 1
grid columnconfigure $frm 1 -weight 1</span>

#
# Manage the frame
#
pack $frm -expand yes -fill both -padx 10 -pady 10

. . .
</pre>
  </blockquote>

  <p>The file <code>ScrolledTablelist2.tcl</code> in the <code>demos</code>
  directory replaces the rather technical code above with just a few lines
  (shown below in <span>red</span> color), by embedding the tablelist into a
  scrollarea widget.&nbsp; It requires Tablelist version 6.5, which is needed
  so the <code><a href=
  "scrollarea.html#respectheader">-respectheader</a></code> and
  <code><a href=
  "scrollarea.html#respecttitlecolumns">-respecttitlecolumns</a></code>
  scrollarea options can work as expected (for earlier Tablelist versions these
  options are silently ignored).&nbsp; As a further benefit, the scrollbars
  created with this method will have the default display mode
  <code>dynamic</code>.</p>

  <blockquote>
    <pre>
package require tablelist_tile 6.5
<span>package require scrollutil_tile</span>

wm title . "Scrolled Tablelist"

#
# Create the tablelist within a scrollarea
#
set f  [ttk::frame .f]
<span>set sa [scrollutil::scrollarea $f.sa]</span>
set tbl $sa.tbl
tablelist::tablelist $tbl ...
. . .
<span>$sa setwidget $tbl</span>

. . .

#
# Manage the scrollarea
#
pack $sa -expand yes -fill both -padx 10 -pady 10

. . .
</pre>
  </blockquote>

  <h3 id="ex_SyncListboxes">Synchronizing Two listbox Widgets</h3>

  <p>The file <code>SyncListboxes.tcl</code> in the <code>demos</code>
  directory creates two listboxes within a <a href=
  "scrollsync.html">scrollsync</a> widget, which in turn is embedded into a
  <a href="scrollarea.html">scrollarea</a>.</p>

  <blockquote>
    <img src="SyncListboxes.png" alt="SyncListboxes" width="321" height="303">
  </blockquote>

  <p>Here is the relevant code, in which the lines related to the scrollarea
  and scrollsync widgets are shown in <span>red</span> color:</p>

  <blockquote>
    <pre>
<span>package require scrollutil_tile</span>

wm title . "European Countries"

. . .

set f  [ttk::frame .f]

. . .

#
# Create a scrollsync widget within a scrollarea
#
<span>set sa [scrollutil::scrollarea $f.sa]
set ss [scrollutil::scrollsync $sa.ss]
$sa setwidget $ss</span>

#
# Populate the scrollsync widget with two listboxes
#

. . .

set lb1 [listbox $ss.lb1 -activestyle none -highlightthickness 0 -width 16]
set lb2 [listbox $ss.lb2 -activestyle none -highlightthickness 0 -width 16]
<span>$ss setwidgets [list $lb1 $lb2]</span>

. . .

grid $lb1 $lb2 -sticky news -padx {0 2}
grid rowconfigure    $ss 0   -weight 1
grid columnconfigure $ss all -weight 1

. . .

pack $sa -side top -expand yes -fill both -padx 10 -pady {2 10}

. . .
</pre>
  </blockquote>

  <h3 id="ex_SyncTablelists">Synchronizing Three tablelist Widgets</h3>

  <p>The file <code>SyncTablelists.tcl</code> in the <code>demos</code>
  directory creates three tablelists within a <a href=
  "scrollsync.html">scrollsync</a> widget, which in turn is embedded into a
  <a href="scrollarea.html">scrollarea</a>.</p>

  <blockquote>
    <img src="SyncTablelists.png" alt="SyncTablelists" width="549" height=
    "342">
  </blockquote>

  <p>The relevant code is similar to the one shown in the <a href=
  "#ex_SyncListboxes">previous example</a>:</p>

  <blockquote>
    <pre>
package require tablelist_tile
<span>package require scrollutil_tile</span>

wm title . "Synchronized Tablelists"

. . .

set f  [ttk::frame .f]

. . .

#
# Create a scrollsync widget within a scrollarea
#
<span>set sa [scrollutil::scrollarea $f.sa]
set ss [scrollutil::scrollsync $sa.ss]
$sa setwidget $ss</span>

#
# Populate the scrollsync widget with three tablelists
#

option add *Tablelist.stripeBackground  #f0f0f0

for {set n 1; set colWidth 40} {$n <= 3} {incr n; incr colWidth 20} {
    set tbl [tablelist::tablelist $ss.tbl$n \
             -columns [list 0 "Column 0" left  $colWidth "Column 1" left]]
    set tbl$n $tbl

    for {set i 0} {$i < 40} {incr i} {
        $tbl insert end [list "cell $i,0" "cell $i,1"]
    }
}
<span>$ss setwidgets [list $tbl1 $tbl2 $tbl3]</span>

grid $tbl1 $tbl2 $tbl3 -sticky news -padx {0 2}
grid rowconfigure    $ss 0   -weight 1
grid columnconfigure $ss all -weight 1

. . .

pack $sa -side top -expand yes -fill both -padx 10 -pady {2 10}

. . .
</pre>
  </blockquote>

  <p>Notice that column #1 of the three tablelist widgets is 40, 60, and 80
  characters wide, respectively.&nbsp; For this reason, when scrolling
  horizontally to the right, the left table's view will reach its horizontal
  end position first, then that of the midde table, and as last one the view of
  the right table.</p>

  <h3 id="ex_ScrollableFrameDemo1">A Script Using a BWidget ScrollableFrame
  Widget</h3>

  <p>The file <code>ScrollableFrmDemo1.tcl</code> in the <code>demos</code>
  directory creates a BWidget ScrollableFrame embedded into a <a href=
  "scrollarea.html">scrollarea</a> widget, creates mouse wheel event bindings
  for the binding tag "all" with the aid of the <code><a href=
  "wheelEvent.html#create">scrollutil::createWheelEventBindings</a></code>
  command, and invokes the <code><a href=
  "wheelEvent.html#enable">scrollutil::enableScrollingByWheel</a></code>
  command for this ScrollableFrame, thus registering the latter for scrolling
  by these bindings.&nbsp; After that it populates the ScrollableFrame with
  ttk::label widgets displaying the names of the European countries,
  ttk::combobox widgets for selecting the corresponding capital cities, and
  ttk::button widgets of style <code>Toolbutton</code> for the less patient
  users, displaying the text "Resolve".</p>

  <blockquote>
    <img src="ScrollableFrmDemo1.png" alt="ScrollableFrmDemo1" width="403"
    height="362">
  </blockquote>

  <p>Here is the relevant code:</p>

  <blockquote>
    <pre>
package require Tk 8.5
package require BWidget
Widget::theme yes
<span>package require scrollutil_tile</span>

wm title . "European Capitals Quiz"

#
# Create a ScrollableFrame within a scrollarea
#
set f  [ttk::frame .f]
<span>set sa [scrollutil::scrollarea $f.sa]</span>
set sf [ScrollableFrame $sa.sf]
<span>$sa setwidget $sf</span>

. . .

#
# Create mouse wheel event bindings for the binding tag "all" and
# register the ScrollableFrame for scrolling by these bindings
#
<span>scrollutil::createWheelEventBindings all
scrollutil::enableScrollingByWheel $sf</span>

#
# Get the content frame and populate it
#

set cf [$sf getframe]

set countryList {
    Albania Andorra Austria Belarus Belgium "Bosnia and Herzegovina" Bulgaria
    . . .
}
set capitalList {
    Tirana "Andorra la Vella" Vienna Minsk Brussels Sarajevo Sofia
    . . .
}

. . .

set capitalList [lsort $capitalList]

. . .

set row 0
foreach country $countryList {
    . . .

    set w [ttk::combobox $cf.cb$row -state readonly -width 14 \
           -values $capitalList]
    . . .

    #
    # Adapt the handling of the mouse wheel events for the ttk::combobox widget
    #
    <span>scrollutil::adaptWheelEventHandling $w</span>

    . . .

    incr row
}

. . .
</pre>
  </blockquote>

  <p>We invoke the <code><a href=
  "wheelEvent.html#adapt">scrollutil::adaptWheelEventHandling</a></code>
  command for every ttk::combobox widget, which is needed for a user-friendly
  event handling, being that this widget has built-in bindings for the mouse
  wheel events.</p>

  <h3 id="ex_ScrolledFrameDemo1">A Script Using an iwidgets::scrolledframe
  Widget</h3>

  <p>The file <code>ScrolledFrmDemo1.tcl</code> in the <code>demos</code>
  directory creates an iwidgets::scrolledframe widget, creates mouse wheel
  event bindings for the binding tag "all" with the aid of the <code><a href=
  "wheelEvent.html#create">scrollutil::createWheelEventBindings</a></code>
  command, and invokes the <code><a href=
  "wheelEvent.html#enable">scrollutil::enableScrollingByWheel</a></code>
  command for this scrolledframe, thus registering the latter for scrolling by
  these bindings.&nbsp; After that it populates the scrolledframe with the same
  widgets as <code>ScrollableFrmDemo1.tcl</code> in the <a href=
  "#ex_ScrollableFrameDemo1">previous example</a>.</p>

  <p>Here is the relevant code:</p>

  <blockquote>
    <pre>
package require Tk 8.5
if {[catch {package require iwidgets} result1] != 0 &&
    [catch {package require Iwidgets} result2] != 0} {
    error "$result1; $result2"
}
source scrolledwidgetPatch.itk                  ;# adds ttk::scrollbar widgets
<span>package require scrollutil</span>

wm title . "European Capitals Quiz"

. . .

#
# Create a scrolledframe
#
set f  [ttk::frame .f]
set sf [iwidgets::scrolledframe $f.sf -borderwidth 1 -relief sunken \
        -scrollmargin 0]
. . .

#
# Create mouse wheel event bindings for the binding tag "all"
# and register the scrolledframe for scrolling by these bindings
#
<span>scrollutil::createWheelEventBindings all
scrollutil::enableScrollingByWheel $sf</span>

#
# Get the content frame and populate it
#

set cf [$sf childsite]
. . .

<i>&lt;exactly as in the previous example&gt;</i>

. . .
</pre>
  </blockquote>

  <h3 id="ex_ScrollableFrameDemo2">A Script Using Two BWidget ScrollableFrame
  Widgets</h3>

  <p>The script <code>ScrollableFrmDemo2.tcl</code> in the <code>demos</code>
  directory creates a BWidget ScrollableFrame embedded into a <a href=
  "scrollarea.html">scrollarea</a> widget and then <code>source</code>s the
  script <code>ScrollableFrmContent.tcl</code>, which populates the
  ScrollableFrame with the following widgets:</p>

  <ul>
    <li>a series of ttk::label widgets;</li>
    <li>a scrolled text widget <code>$txt</code> within a scrollarea;</li>
    <li>a scrolled listbox widget <code>$lb</code> within a scrollarea;</li>
    <li>a ttk::combobox widget <code>$cb</code>;</li>
    <li>a ttk::spinbox widget <code>$sb</code>;</li>
    <li>a ttk::entry widget;</li>
    <li>a ttk::separator widget;</li>
    <li>a mentry widget <code>$me</code> of type <code>"Date"</code>;</li>
    <li>a scrolled tablelist widget <code>$tbl</code> within a scrollarea;</li>
    <li>a scrolled ttk::treeview widget <code>$tv</code> within a
    scrollarea.</li>
  </ul>

  <p>With the exception of ttk::label, ttk::entry, and ttk::separator, all
  these widgets have bult-in mouse wheel event bindings.</p>

  <blockquote>
    <img src="ScrollableFrmDemo2.png" alt="ScrollableFrmDemo2" width="607"
    height="513">
  </blockquote>

  <p>Here is the relevant code:</p>

  <blockquote>
    <pre>
package require Tk 8.5.9                        ;# for ttk::spinbox
package require BWidget
Widget::theme yes
package require mentry_tile 3.2                 ;# for mouse wheel support
package require tablelist_tile 6.5              ;# for -(x|y)mousewheelwindow
                                                ;# and scrollutil::scrollarea
<span>package require scrollutil_tile</span>

wm title . "Scrollutil Demo"

#
# Create a ScrollableFrame within a scrollarea
#
set tf [ttk::frame .tf]
<span>set sa [scrollutil::scrollarea $tf.sa]</span>
set sf [ScrollableFrame $sa.sf]
<span>$sa setwidget $sf</span>

. . .

#
# Get the content frame and populate it
#
set cf [$sf getframe]
source ScrollableFrmContent.tcl
</pre>
  </blockquote>

  <p>And here is the additional stuff related to the mouse wheel events, using
  the Scrollutil commands described in the <a href="#ov_what">What Is
  Scrollutil?</a> section:</p>

  <blockquote>
    <pre>
#
# Create mouse wheel event bindings for the binding tag "all" and
# register the ScrollableFrame for scrolling by these bindings
#
<span>scrollutil::createWheelEventBindings all
scrollutil::enableScrollingByWheel $sf</span>

#
# Adapt the handling of the mouse wheel events for the text, listbox,
# ttk::combobox, ttk::spinbox, tablelist, and ttk::treeview widgets, as
# well as for the entry components of the mentry widget of type "Date"
#
set entryList [$me entries]
<span>scrollutil::adaptWheelEventHandling $txt $lb $cb $sb $tbl $tv {*}$entryList</span>

#
# For the entry components of the mentry widget
# set the "focus check window" to the mentry
#
<span>scrollutil::setFocusCheckWindow {*}$entryList $me</span>
</pre>
  </blockquote>

  <p>Notice that we have passed, among others, the tablelist widget to the
  <code><a href=
  "wheelEvent.html#adapt">scrollutil::adaptWheelEventHandling</a></code>
  command.&nbsp; This will only work for Tablelist versions 6.4 and later,
  because the command handles tablelist widgets by setting their
  <code>-xmousewheelwindow</code> and <code>-ymousewheelwindow</code> options
  to the path name of the containing toplevel window, and these options were
  introduced in Tablelist version 6.4.&nbsp; (For earlier Tablelist versions
  the command silently ignores any tablelist widget passed to it as
  argument.)</p>

  <p>As already mentioned, in the file <code>ScrollableFrmContent.tcl</code>
  the scrolled text, listbox, tablelist, and ttk::treeview widgets are created
  within <a href="scrollarea.html">scrollarea</a> widgets:</p>

  <blockquote>
    <pre>
<span>set _sa [scrollutil::scrollarea ...]</span>
set txt [text $_sa.txt -font TkFixedFont -width 73]
<span>$_sa setwidget $txt</span>
grid $_sa ...

. . .

<span>set _sa [scrollutil::scrollarea ...]</span>
set lb [listbox $_sa.lb -width 0]
<span>$_sa setwidget $lb</span>
grid $_sa ...

. . .

<span>set _sa [scrollutil::scrollarea ...]</span>
set tbl [tablelist::tablelist $_sa.tbl ...]
. . .
<span>$_sa setwidget $tbl</span>
grid $_sa ...

. . .

<span>set _sa [scrollutil::scrollarea ... -borderwidth 0]</span>
set tv [ttk::treeview $_sa.tv ...]
. . .
<span>$_sa setwidget $tv</span>
grid $_sa ...
</pre>
  </blockquote>

  <p>In the case of the text, listbox, and tablelist widgets we use scrollarea
  widgets with their default&nbsp; <code>-borderwidth 1 -relief
  sunken</code>&nbsp; settings, which will cause the <code><a href=
  "scrollarea.html#setwidget">setwidget</a></code> subcommand of the associated
  Tcl commands to set the <code>-borderwidth</code> option of the text,
  listbox, and tablelist widgets to <code>0</code>.&nbsp; On the other hand,
  for the ttk::treeview we use a scrollarea widget with&nbsp;
  <code>-borderwidth 0</code>,&nbsp; because the ttk::treeview has a border of
  width <code>1</code> and doesn't support the <code>-borderwidth</code>
  configuration option.</p>

  <p>The file <code>ScrollableFrmContent.tcl</code> contains also the
  implementation of the procedure <code>configTablelist</code>, associated with
  the "Configure Tablelist Widget" button as the value of its
  <code>-command</code> option.&nbsp; This procedure opens a toplevel window
  that contains a BWidget ScrollableFrame embedded into a <a href=
  "scrollarea.html">scrollarea</a> widget and invokes the
  <code><a href=
  "wheelEvent.html#enable">scrollutil::enableScrollingByWheel</a></code>
  command for this ScrollableFrame, thus registering the latter for scrolling
  by the already created mouse wheel event bindings for the binding tag
  <code>"all"</code>.&nbsp; After that it populates the ScrollableFrame with
  ttk::label, ttk::combobox, ttk::spinbox, ttk::entry, and ttk::checkbutton
  widgets used to display and edit the configuration options of the tablelist
  widget.&nbsp; Whenever a ttk::combobox or ttk::spinbox is created, the
  <code>scrollutil::adaptWheelEventHandling</code> command is invoked for it,
  being that these widgets have built-in bindings for the mouse wheel
  events.</p>

  <blockquote>
    <img src="TablelistConfig.png" alt="TablelistConfig" width="379"
    height="362">
  </blockquote>

  <h3 id="ex_ScrolledFrameDemo2">A Script Using Two iwidgets::scrolledframe
  Widgets</h3>

  <p>The script <code>ScrolledFrmDemo2.tcl</code> in the <code>demos</code>
  directory creates an iwidgets::scrolledframe widget and then
  <code>source</code>s the file <code>ScrolledFrmContent.tcl</code>, which
  populates the scrolledframe with the same widgets as
  <code>ScrollableFrmContent.tcl</code> in the <a href=
  "#ex_ScrollableFrameDemo2">previous example</a>.</p>

  <p>Here is the relevant code:</p>

  <blockquote>
    <pre>
package require Tk 8.5.9                        ;# for ttk::spinbox
if {[catch {package require iwidgets} result1] != 0 &&
    [catch {package require Iwidgets} result2] != 0} {
    error "$result1; $result2"
}
source scrolledwidgetPatch.itk                  ;# adds ttk::scrollbar widgets
package require mentry_tile 3.2                 ;# for mouse wheel support
package require tablelist_tile 6.5              ;# for -(x|y)mousewheelwindow
                                                ;# and scrollutil::scrollarea
<span>package require scrollutil_tile</span>

wm title . "Scrollutil Demo"

. . .

#
# Create a scrolledframe
#
set tf [ttk::frame .tf]
set sf [iwidgets::scrolledframe $tf.sf -borderwidth 1 -relief sunken \
        -scrollmargin 0]
. . .

#
# Get the content frame and populate it
#
set cf [$sf childsite]
. . .
source ScrolledFrmContent.tcl
</pre>
  </blockquote>

  <p>The additional stuff related to the mouse wheel events contains exactly
  the same Scrollutil command invocations as the one in the previous
  example.</p>

  <p>The file <code>ScrolledFrmContent.tcl</code> contains also the
  implementation of the procedure <code>configTablelist</code>, associated with
  the "Configure Tablelist Widget" button as the value of its
  <code>-command</code> option.&nbsp; This procedure opens a toplevel window
  that contains an iwidgets::scrolledframe widget and invokes the
  <code><a href=
  "wheelEvent.html#enable">scrollutil::enableScrollingByWheel</a></code>
  command for this scrolledframe, thus registering the latter for scrolling by
  the already created mouse wheel event bindings for the binding tag
  <code>"all"</code>.&nbsp; After that it populates the scrolledframe with
  ttk::label, ttk::combobox, ttk::spinbox, ttk::entry, and ttk::checkbutton
  widgets used to display and edit the configuration options of the tablelist
  widget.&nbsp; Whenever a ttk::combobox or ttk::spinbox is created, the
  <code><a href=
  "wheelEvent.html#adapt">scrollutil::adaptWheelEventHandling</a></code>
  command is invoked for it, being that these widgets have built-in bindings
  for the mouse wheel events.</p>

  <p>Again, all this is nearly identical to what we did in the previous
  example.</p>

  <div align="center">
    <p><a href="#contents">Contents</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href=
    "index.html">Start page</a></p>
  </div>
</body>
</html>
